#!/usr/bin/env python
# -*- coding: UTF-8 -*-

__author__ = u"samssrus <samssrus@gmail.com>"
__license__ = u"Программа распространяется по лицензии GNU GPL v3. Подробности в файле LICENSE."
__date__ = "$13.05.2011 13:24:11$"
__version__ = "0.1.2"

u"""Simple server starter - простой графическй интерфейс для запуска сервера 
Apache (LAMP, lighttpd), написанный на языке Python (с использованием библиотеки 
PyQt)."""

import logging
import os.path
import subprocess
import sys
from res.configobj import ConfigObj
from res.i18n import gettext
try:
    from PyQt4 import QtCore, QtGui
except ImportError:
    sys.exit(u'Ошибка импорта. Установите PyQt4.')

class MainWidget(QtGui.QWidget):
    u"""MainWidget - класс создания основного окна программы."""

    SERVER_STARTED = 1 #флаг статуса, что сервер запущен
    SERVER_STOPPED = 0 #флаг статуса, что сервер остановлен

    #массив состояний кнопок
    btn_state = {'blanc':       {   'btnStart':1,
                                    'btnStop':0,
                                    'btnRestart':0},
                 'start':       {   'btnStart':0,
                                    'btnStop':1,
                                    'btnRestart':1},
                 'stop':        {   'btnStart':1,
                                    'btnStop':0,
                                    'btnRestart':0},
                 'restart':     {   'btnStart':0,
                                    'btnStop':1,
                                    'btnRestart':1},
		 'block':       {   'btnStart':0,
                                    'btnStop':0,
                                    'btnRestart':0},
		 'status':      {}
		}
    
    def __init__(self, parent=None):
        super(MainWidget, self).__init__(parent)
        self.setWindowFlags(QtCore.Qt.Window | QtCore.Qt.WindowMinimizeButtonHint | QtCore.Qt.WindowCloseButtonHint)
        self.setAttribute(QtCore.Qt.WA_QuitOnClose, True)
        #устанавливаем заголовок окна
        self.setWindowTitle(gettext('Server starter'))
        #устанавливаем размеры окна
        self.resize(270, 350)
        #устанавливаем base_path
        base_path = os.path.dirname(os.path.realpath(__file__))
        #устанавливаем картинку программы
        self.setWindowIcon(QtGui.QIcon(base_path + '/res/img/icon.ico'))
        #выравниваем окно по центру экрана
        self.centre()	
        #настраиваем лог
        logging.basicConfig(
            filename=base_path + '/res/logfile.log',
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-3s in %(filename)s->%(module)s->%(funcName)s: %(lineno)s %(message)s'
        )
        self.log = logging.getLogger('app')
        #читаем файл конфигурации (используя стороннюю библиотеку ConfigObj)
        self.config = ConfigObj(base_path + '/res/config.ini', encoding='UTF8')
        self.current_config = self.config['default']['current_config']
        self.server_path = self.config[self.current_config]['server_path']
        self.set_cmd() #вызываем функцию установки команд из файла конфигурации
        #устанавливаем начальный статус - сервер остановлен
        self.status = self.SERVER_STOPPED
        #вызываем функцию создания основных элементов управления
        self.create_gui()

    def centre(self):
        u"""функция для выравнивания окна программы по центру экрана."""
        screen = QtGui.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width() - size.width()) / 2, (screen.height() - size.height()) / 2)

    def create_gui(self):
        u"""Функция создания основных элементов управления (всех кнопок)."""
        #окно вывода сообщений
        self.txtStatus = QtGui.QTextEdit()
        #кнопка запуска сервера
        self.btnStart = QtGui.QPushButton(gettext('Start'))
        self.btnStart.setToolTip(gettext('Start server'))
        self.connect(self.btnStart, QtCore.SIGNAL('clicked()'), self.click_start)
        #кнопка остановки сервера
        self.btnStop = QtGui.QPushButton(gettext('Stop'))
        self.btnStop.setToolTip(gettext('Stop server'))
        self.connect(self.btnStop, QtCore.SIGNAL('clicked()'), self.click_stop)
        #кнопка перезапуска сервера
        self.btnRestart = QtGui.QPushButton(gettext('Restart'))
        self.btnRestart.setToolTip(gettext('Restart server'))
        self.connect(self.btnRestart, QtCore.SIGNAL('clicked()'), self.click_restart)
        #напоминание об использовании прав пользователя root для выполнения команд
        lblNote = QtGui.QLabel(gettext('Need privelegy as ') + ' <b>root</b>.')
        lblNote.setWordWrap(1)
        #кнопка получения сведений о состоянии сервера
        btnStatus = QtGui.QPushButton(gettext('Status'))
        btnStatus.setToolTip(gettext('Get server status'))
        self.connect(btnStatus, QtCore.SIGNAL('clicked()'), self.click_status)
        #кнопка закрытия программы
        btnClose = QtGui.QPushButton(gettext('Exit'))
        btnClose.setToolTip(gettext('Exit application'))
        self.connect(btnClose, QtCore.SIGNAL('clicked()'), QtCore.SLOT('close()'))
        #выставляем начальное состояние кнопок
        self.set_btn_state('blanc')

        #располагаем элементы в окне
        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(self.btnStart)
        vbox.addWidget(self.btnStop)
        vbox.addWidget(self.btnRestart)
        vbox.addStretch(1)
        #проверка установлен ли сервер путем проверки существования пути
        if not os.path.exists(self.server_path):
            message = u'<b style="color: red">' + gettext('Server not found. Check path to server.') + '</b>'
            self.log.error(message)
            self.txtStatus.append(message)
            self.set_btn_state('block')
        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(btnStatus)
        hbox.addStretch(1)
        hbox.addWidget(btnClose)
        vbox.addWidget(self.txtStatus)
        vbox.addWidget(lblNote)
        vbox.addLayout(hbox)
        self.setLayout(vbox)

    def set_cmd(self):
        u"""Устанавливаем команды из файла конфигурации"""
        #команда запуска сервера
        self.cmd_start = self.config[self.current_config]['start']
        #команда остановки сервера
        self.cmd_stop = self.config[self.current_config]['stop']
        #команда перезапуска сервера
        self.cmd_restart = self.config[self.current_config]['restart']
        #команда получения статуса
        self.cmd_status = self.config[self.current_config]['status']

    def click_start(self):
        u"""Функция вызова команды по запуску сервера."""        
        self.set_btn_state('start')	
        self.run_cmd(self.cmd_start)
        self.status = self.SERVER_STARTED

    def click_stop(self):
        u"""Функция вызова команды по остановке сервера."""        
        self.set_btn_state('stop')        
        self.run_cmd(self.cmd_stop)
        self.status = self.SERVER_STOPPED

    def click_restart(self):
        u"""Функция вызова команды по перезапуску сервера."""        
        self.set_btn_state('restart')        
        self.run_cmd(self.cmd_restart)
        self.status = self.SERVER_STARTED

    def click_status(self):
        u"""Функция вызова команды status сервера."""        
        self.run_cmd(self.cmd_status)

    def set_btn_state(self, state):
        u"""Функция выставления состояния кнопок (доступна / недоступна)."""
        states = self.btn_state[state]
        for btn, enabled in states.iteritems():
            obj = getattr(self, btn)
            obj.setEnabled(enabled)

    def run_cmd(self, cmd):
        u"""Функция выполнения команд по запуску, остановке и перезапуску сервера и др."""
        QtGui.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor) #тип курсора ожидание
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (out, err) = (p.stdout.read(), p.stderr.read())
        message = ''
        if err:
            message = u'<b style="color: red">' + gettext('Error in command') + ' %s:\n%s</b>' % (cmd, str(err))
            self.log.error(message)
        else:
            message = u'<b style="color: green">' + gettext('Executed') + ':\n%s</b>' % str(out)
        self.txtStatus.append(message)        
        QtGui.QApplication.restoreOverrideCursor() #возврат к нормальному типу курсора

    def closeEvent(self, event):
        u"""Функция контроля за закрытием программы."""
        if self.status is not self.SERVER_STOPPED:
            #если сервер запущен показать диалоговое окно подтверждения выхода
            dlgConfirmExit = QtGui.QMessageBox(self)
            dlgConfirmExit.setIcon(QtGui.QMessageBox.Question)
            dlgConfirmExit.setWindowTitle(gettext('Attention!'))
            dlgConfirmExit.setText(gettext('Do you want exit?'))
            btnExitYes = QtGui.QPushButton(gettext('Out'))
            btnExitNo = QtGui.QPushButton(gettext('Back'))
            dlgConfirmExit.addButton(btnExitYes, QtGui.QMessageBox.YesRole)
            dlgConfirmExit.addButton(btnExitNo, QtGui.QMessageBox.NoRole)
            dlgConfirmExit.exec_()
            if dlgConfirmExit.clickedButton() == btnExitYes:
                #если выход подтвержден, то предложить остановить сервер
                dlgConfirmStopServer = QtGui.QMessageBox(self)
                dlgConfirmStopServer.setIcon(QtGui.QMessageBox.Question)
                dlgConfirmStopServer.setWindowTitle(gettext('Question'))
                dlgConfirmStopServer.setText(gettext('Do you want stop server?'))
                btnStopYes = QtGui.QPushButton(gettext('Stop the server'))
                btnStopNo = QtGui.QPushButton(gettext('Dont stop the server'))
                dlgConfirmStopServer.addButton(btnStopYes, QtGui.QMessageBox.YesRole)
                dlgConfirmStopServer.addButton(btnStopNo, QtGui.QMessageBox.NoRole)
                dlgConfirmStopServer.setDefaultButton(btnStopYes)
                dlgConfirmStopServer.exec_()
                if dlgConfirmStopServer.clickedButton() == btnStopYes:
                    self.run_cmd(self.cmd_stop) #выполняем команду остановки сервера
                event.accept()
            else:
                event.ignore()
        else:
            event.accept()

def main():
    u"""Функция запуска основного цикла программы"""
    application = QtGui.QApplication(sys.argv)
    window = MainWidget()
    window.show()
    sys.exit(application.exec_())

if __name__ == "__main__":
    main()

