# -*- coding: UTF-8 -*-
u"""
Модуль i18n - модуль помощник для облегчения локализации

Повзаимствовано из http://www.slideshare.net/gotsyk/internationalization-and-localization-of-the-python-applications-with-gettext-by-alexandr-belchenko

Для генерации шаблонов:
    xgettext myapp.py -o application.pot
    msginit -l ru -i application.pot -o application-ru.po    (поменять на charset=UTF-8 в файле application-ru.po)
    msgfmt application-ru.po -o application.mo

Далее разместить файлы:
    locale/en/LC_MESSAGES/application.mo
    locale/ru/LC_MESSAGES/application.mo

Примеры правильного использования:

    from i18n import gettext

    s = gettext('Start')
    или
    s = gettext('Page %(number)d of %(count)d pages') % dict(number = number, count = count)

    s = ngettext('Found %d document','Found %d documents',n) % n
    при этом в файле локализации:
    msgid 'Found %d document'
    msgid_plural 'Found %d documents'
    msgstr[0] 'Найден %d документ'      (Найден 1, 21, 31 документ)
    msgstr[1] 'Найдено %d документа'	(Найдено 2, 3, 4, 22, 23, 24, 32, 33, 34 документа)
    msgstr[2] 'Найдено %d документов'	(Найдено 0, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25, 26, 27, 28, 29, 30, 35, 36 документов)
"""
__version__="0.1.0"

import gettext as _gettext
import os.path

#устанавливаем base_path (нужно для установки правильного пути к файлам локализации)
base_path = os.path.dirname( os.path.realpath( __file__ ) )

_t = _gettext.NullTranslations
_t = _gettext.translation("application", localedir=base_path + "/locale/", fallback=True)

def gettext(text):
    #основная функция для получения переведенных значений
    return _t.ugettext(text)

def ngettext(text, plural, number):
    #функция для получения значений во множественном числе
    return _t.ungettext(text, plural, number)

def N_(text):
    #функция для получения не переведенного значения
    return text